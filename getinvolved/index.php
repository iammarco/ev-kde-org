<?php
 $page_title = "Get Involved";
 include "header.inc";
?>

<p>If you are a member of the KDE community who has contributed to promoting, 
developing or documenting KDE or doing any other KDE related work, you can 
join the KDE e.V. as an <a 
href="/corporate/statutes.php#4">active member</a>. See the <a 
href="members.php">instructions how to become a member</a> for more
details.</p>

<p>If you are a corporation and want to support KDE you can become a <a
href="supporting-members.php">supporting member</a>.</p>

<p>If you want to support KDE by donating, consider becoming a Supporting 
Member through our <a href="http://jointhegame.kde.org">Join the Game</a> program.

<?php
include "footer.inc";
?>
