<?php
 $page_title = "KDE e.V. Working Groups";
 include "header.inc";
?>

<h2>Active Working Groups</h2>

<ul>
  <li><a href="abwg.php">Advisory Board Working Group</a></li>

  <li><a href="cwg.php">Community Working Group (CWG)</a></li>

  <li><a href="fwg.php">Financial Working Group (FiWG)</a></li>

  <li><a href="fundraisingwg.php">Fundraising Working Group (FuWG)</a></li>
  
  <li><a href="sysadmin.php">System Administration (sysadmin)</a></li> 
</ul>

<?php
 include "footer.inc";
?>
