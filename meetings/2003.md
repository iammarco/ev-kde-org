---
title: "Meeting Minutes of the KDE e.V. General Assembly 2003"
layout: page
---

# Meeting Minutes of the KDE e.V. General Assembly 2003

<p>	Nove Hrady, Czech Republic, August 22nd, 2003</p>
<p>09:00 AM: Start of the regular membership assembly at the Nove Hrady castle</p>
<p>09:01 AM: Assessment of duly Invitation</p>
<p>09:02 AM: Assessment of ability to vote, 47 active members present. </p>
<p>09:03 AM: Election of chairman and secretary per by acclamation in unison.</p>
<p> Assembly chairman: Kalle Dalheimer,<br /> Secretary: Ralf Nolden </p>
<p>09:04 AM: Election of treasure auditors: Stephan Binner, Lars Knoll </p>
<p>09:05 AM: Election of the election supervisor in unison: Matthias Ettrich </p>
<p>09:10 AM: Report of the Board</p>
<p>After the board introduced itself one-by-one, reports of the different activities were made. Especially the presence of the KDE e.V. At different fairs and tradeshows was 	pointed out: LinuxWord Conference &amp; Expo, Frankfurt, CeBIT 2003 Hannover, LinuxTag 2003 Karsruhe, Berliner Infotage, LinuxTag 2003 Chemnitz. Furthermore KDE e.V. Has helped creating its own GNU/Linux Live-CD based on Debian GNU/Linux, of which Danka has provided 500 copies. To distribute them on fairs and tradeshows. Moreover, serveral T-Shirts have been created in cooperation with Kernelkoncepts. Likewise, the planning of this years membership assembly and the attached conference and hackfest “Kastle” was planned and conducted by KDE e.V. Tresurer Mirko Böhm gave a financial report which gave a rough overview about the financial situation of the association.</p>
<p>Amongst other, a credit entry by IBM was used to buy hardware for the operation of 	KDE's primary server from IBM. Miscellaneous  equipment and parts for fairs have been bought and travel costs for members to fairs or the last KDE meeting 2002 in Hamburg have been compensated. </p>
<p>During the course of the meeting, Kalle Dalheimer reported about the current state of the Trademark registration of KDE for the KDE e.V.</p>
<p>9:30 AM: Report about the KDE League by Daniel Molkentin, Chris Schläger and Scott Wheeler</p>
<p>This report gave insight about the motivation to found the KDE League in 2001 and made clear that the League is currently not active. The representatives of KDE e.V. pointed out that it would make sense to dissolve the league.</p>
<p>	09:45 AM: Report about the KDE Free Qt Foundation from Cornelius Schumacher  and 	Martin Konold: As the representatives of KDE e.V. in the KDE Free Qt Foundation they 	reported no problems. The question was raised wether the foundation was still needed and wether the contract is still valid. Both questions were affirmed and the were no endorsements with regard to the KDE FreeQt Foundation.</p>
<p>10:00 AM: Changes to the by-laws</p>
<p>	For the voting about the by-law changes listed in the invitation, the respective changes 	were split in four groups. Those were separately presented, explained and put for vote. The changes and their voting result are listed in the following:</p>
<p>		1. The English translation of the association is “KDE Incorporated”,  abbreviated “KDE, Inc”</p>
<p>Yes-Votes: 24<br />No-Votes: 20<br />Abstentions:  3</p>
<p>Therefor, the proposal was dismissed.</p>
<p>2. Publication about the decisions of the board and the membership assembly</p>
<p>Yes-Votes: 23<br />No-Votes: 17<br />Abstentions: 7</p>
<p>Therefor, the proposal was dismissed.</p>
<p>3. Adding a paragraph about the purpose of the delegates for the KDE FreeQt Foundation and the KDE League</p>
<p>Yes-Votes: 3<br />No-Votes: 41<br />Abstentions: 4</p>
<p>Therefor, the proposal was dismissed.</p>
<p>4. Vote about the rest of the proposals as listed in the invitation:</p>
<p>Yes-Votes: 46<br />No-Votes: 0<br />Abstentions: 1</p>
<p>Therefor, all requested by-law changes as listed in the inivation were confirmed, with exception of the above-listed points 1-3, which were put on vote separately.</p>
<p>1:00 PM: Break</p>
<p>2:00 PM: Report of the treasure auditors Stephan Binner and Lars Knoll. The audit 	found no discrepancies. All documents about investments and expenses exist and have been booked accordingly.</p>
<p>2:10 PM: Relief of the Board . The Board was relieved with 42 yes-votes: and 5<br />abstentions. </p>
<p>2:15 PM: Miscellaneous</p>
<p>Marc Mutz gave a report about the activities of the FLA and the Free Software Foundation Europe that could be useful for KDE e.V. Matthias Ettrich requested a discussion about that between the board and the FSF Europe. This request gained 36 yes votes, two no votes and 9 abstentions.</p>
<p>Cornelius Schumacher asked how to conduct the transfer of Internet domains to KDE 	e.V. Martin Konold suggested to establish a process for the transferring domains to KDE e.V. Chris Schläger finally requested that the board should work out a plan for such a project. This was put for vote where 35 voted with yes and 12 abstained. Matthias Ettrich finally asked Martin Konold whether he would transfer the kde.org, currently owned by him, to KDE e.V. Konold declined that.</p>
<p>4:00 PM: All present members (47) signed a confirmation about their presence and participation at the votes about the by-law changes.</p>
<p>4:05 PM: End of assembly</p>
<p><b>Assembly chairman:</b></p>
<p>(Kalle Dalheimer)</p>
<p><b>Secretary:</b></p>
<p>(Ralf Nolden)</p>
