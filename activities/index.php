<?php
 $page_title = "KDE e.V. Activities";
 include "header.inc";
?>

<h2>Akademy</h2>

<p>
Since 2003 the KDE e.V. has organized an annual meeting of the KDE community. It
includes a conference, time for coding, meetings and
the annual membership meeting of the e.V.  It has been known under the name <a
href="http://akademy.kde.org">Akademy</a> since 2004. It is the biggest meeting
of the worldwide KDE community.
</p>
<p><a href="../akademy/index.php">Read more...</a></p>

<h2>Developer Meetings</h2>
<p>The KDE e.V. supports focused developer meetings. These small but intensive
get-togethers of developers to work on dedicated topics for a few days have
proven to be a very efficient tool to get useful and exciting work done. The
e.V. supports these by taking over travel costs and helping with
organization.</p>
<p><a href="devmeetings/index.php">Read more...</a></p>

<h2>Fiduciary Licensing Agreement (FLA)</h2>
<p>Copyright assignment is a personal act. It is entirely optional and at an
individual developer, contributor or copyright holder's discretion whether to
assign copyright to KDE e.V. or not. While there are various legal avenues
available to do such an assignment, a Fiduciary License Agreement is one that
preserves the spirit of Free Software and is easy to administer.</p>
<p><a href="https://ev.kde.org/rules/fla.php">Read more...</a></p>

<h2>Community Partnership Program</h2>
<p>The goal of KDE e.V. is to support free software development, promotion,
and education in general, and KDE specifically. We perceive ourselves to be
part of the greater free software movement, embedded in the broad context of
free culture. While we are dedicated to supporting the KDE community, we also
work together with other communities, organizations and individuals to further
our common goals. The Community Partnership Program provides a framework for
cooperation.</p>
<p><a href="https://ev.kde.org/activities/partnershipprogram.php">Read more...</a></p>

<h2>Events</h2>

<p>The KDE e.V. helps representing KDE at events like trade shows and
conferences. It does this by subsidizing travel costs or by providing material
like the KBoothBox.</p>

<h2>KDE Free Qt Foundation</h2>

<p>The KDE e.V. and Trolltech jointly founded the <a
href="http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt
Foundation</a> in
1998. The purpose of this foundation is to ensure that the Graphical User
Interface Toolkit Qt on which KDE is based will always be available as free
software.</p>

<h2>Trademarks</h2>

<p>The KDE e.V. owns the trademarks on KDE and the K Desktop Environment
logo. They are registered in the United States and the European Union.</p>

<h2>Infrastructure</h2>

<p>The KDE e.V. helps running the infrastructure needed by the KDE community.
This includes servers for SVN, mail, web site, bug tracking, download.</p>

<h2>Legal</h2>

<p>
KDE e.V. handles the legal issues around the KDE project as well.
Its primary instrument is the <a href="../rules/fla.php">Fiduciary
License Agreement</a> through which the copyright on KDE source code
can be consolidated.
The <a href="http://fsfe.org/">Free Software Foundation Europe</a>
provides assistance in legal matters.
</p>

<!-- It would be nice to expand this page with some more detailed information.
This would probably need to split it up in subpages. That's why it is in an own
directory. -->

<?php
include "footer.inc";
?>
