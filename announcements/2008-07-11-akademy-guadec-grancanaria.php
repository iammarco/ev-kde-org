<?php
  $page_title = "KDE and GNOME to Co-locate Flagship Conferences on Gran Canaria in 2009";
  $site_root = "../";
  include "header.inc";
?>

<p>The KDE e.V. and GNOME Foundation today announced that they will hold their
yearly conferences, Akademy and GUADEC in 2009 in Gran Canaria. The conferences
will be separate events, but co-located and hosted by the same organizers, the
Cabildo of Gran Canaria and its Secretary of Tourism, Technological Innovation
and Foreign Trade.</p>

<p>"The GNOME community is very excited about the co-hosted
GUADEC and Akademy" says Behdad Esfahbod, president at the GNOME foundation,
"GUADEC has traditionally been a very important chance for our community to meet
in person, build great working relationships and make new friends. We're looking
forward to having the opportunity to extend those relationships to our KDE
colleagues at Akademy/GUADEC."</p>

<p>KDE e.V.'s vice-president Adriaan de Groot adds
"KDE e.V. is looking forward to a co-located conference, where the GNOME and KDE
communities can mingle and cooperate as never before in one location. Gran
Canaria is uniquely located at the junction of Europe and Africa, close to the
Americas and is a fitting place for a historic 'meet-your-neighbours'
conference." </p>
 
<p>GUADEC and Akademy 2009 will be held on Gran Canaria, an island of the
Canary Islands archipelago. The tentative schedule plans the event from Friday,
July, 3rd until Saturday, July 11th 2009 in the Alfredo Kraus auditorium and the
adjacent Congress Palace in Las Palmas de Gran Canaria.</p>

<p>This co-located event will turn Gran Canaria into the capital of
Freedesktop.org development for a whole week next summer. </p>

<p> While there were other excellent bids, the KDE e.V. and GNOME foundation
have settled on Gran Canaria because of its position as Port to Africa and the
excellent circumstances for holding such an event there. Unfortunately, having
three proposals, two have to be rejected. The proposals from Tampere in Finland
and Coruna in Spain were close contenders. Both foundations would like to thank
those organisers for the work they have put into their proposals and encourage
them to consider their cities for conferences in future years.</p>

<p> The <a href="http://grancanariadesktopmeeting.eslic.es">conference
organiser's Wiki<a/> has extensive information about the planned
conferences on the Canaries.</p>

<p>
For further information, or for media enquiries, please contact
<a href="mailto:board@gnome.org">board@gnome.org</a> and <a
href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.
</p>

<h2>About KDE and the KDE e.V.</h2>

<p>
KDE is an international technology team that creates free and open source
software for desktop and portable computing. Among KDE's products are a modern
desktop system for Linux and UNIX platforms, comprehensive office productivity
and groupware suites and hundreds of software titles in many categories
including Internet and web applications, multimedia, entertainment, educational,
graphics and software development. KDE software is translated into more than 60
languages and is built with ease of use and modern accessibility principles in
mind. KDE4's full-featured applications run natively on Linux, BSD, Solaris,
Windows and Mac OS X.
</p>

<p>
KDE e.V. is the organization that supports the growth of the KDE community. Its
mission statement -- to promote and distribute Free Desktop software -- is
provided through legal, financial and organizational support for the KDE
community. KDE e.V. organises the yearly KDE World Summit "Akademy",
along with numerous smaller-scale development meetings.
</p>

<p>
More information about KDE and the KDE e.V. can be found at <a
href="http://www.kde.org">www.kde.org</a> and <a
href="http://ev.kde.org">ev.kde.org</a>.
</p>

<h2>About GNOME and the GNOME Foundation</h2>

<p>
GNOME is a free-software project whose goal is to develop a complete, accessible
and easy to use desktop for Linux and Unix-based operating systems. GNOME also
includes a complete development environment to create new applications. It is
released twice a year on a regular schedule.
</p>

<p>
The GNOME desktop is used by millions of people around the world. GNOME is a
standard part of all leading GNU/Linux and Unix distributions, and is popular
with both large existing corporate deployments and millions of small business
and home users worldwide.
</p>

<p>
Comprised of hundreds of volunteer developers and industry-leading companies,
the GNOME Foundation is an organization committed to supporting the advancement
of GNOME. The Foundation is a member directed, non-profit organization that
provides financial, organizational and legal support to the GNOME project and
helps determine its vision and roadmap.
</p>

<p>
More information about GNOME and the GNOME Foundation can be found at
<a href="http://www.gnome.org">www.gnome.org</a> and <a
href="http://foundation.gnome.org">foundation.gnome.org</a>.
</p>

<?php
  include("footer.inc");
?>
