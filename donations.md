---
title: "Donations to the KDE e.V."
layout: page
---

## Support KDE e.V.

The KDE e.V. is a registered non-profit organization and accepted as
tax-exempt. Its <a href="corporate/statutes.html">articles of association</a> 
oblige to spend all donations it gets to support the KDE project. You can help 
the KDE project by donating to the KDE e.V. or by becoming a 
<a href="/supporting-members.html">Supporting Member</a>.

### One-time donations

<a href="http://kde.org/community/donations/">Go to donations page.</a>

## Individual Supporting Membership

You can become an individual Supporting Member of KDE e.V. through the Join the 
Game support program. Click on the image below to learn more about it!

<a href="https://jointhegame.kde.org"><img src="images/Jtg.png" class="noborder" style="float: right; margin: 0px; background-image: none; border: 0;" alt="Join the Game and support KDE" /></a>

### Corporate Supporting Membership

Please see our <a href="/getinvolved/supporting-members.html">Supporting Membership information</a> 
page for more information.


## Questions

If you have any questions or are looking for more specific ways to support
KDE please contact the <a href="mailto:kde-ev-board@kde.org">board of the KDE
e.V.</a>

## Donations and taxes

As the KDE e.V. is a not-for-profit organisation (<em>"Gemeinn&uuml;tziger 
Verein"</em>), donations can in Germany be deducted from your tax. Please refer
to <a href="/donations-taxes-de.html">this page</a> for more detailed information. 
(Page is in German.)

### Receipts (Spendenquittungen)

If you need a receipt ("Spendenquittung") for your donation, please contact the
<a href="mailto:kde-ev-treasurer@kde.org">KDE e.V. treasurer</a>.
