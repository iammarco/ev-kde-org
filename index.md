---
title: "KDE e.V. Homepage"
layout: page
---

<img src="images/ev_large.png" class="float-right" style="width: 395px; height: 253px;" alt="KDE e.V. logo" />

KDE e.V. is a registered non-profit organization that represents
the <a href="https://kde.org">KDE Community</a> in legal and financial matters.
Read our <a href="/whatiskdeev.html">mission statement</a>.


On this site you will find [reports](/reports) about past and
[ongoing activities](/activities)  and information how you can
[get involved](/getinvolved).

KDE e.V. is made up of its [members](/members.html), from which the
[board of KDE e.V.](/corporate/board.html) is elected.  KDE e.V. has
[supporting members](/supporting-members.html) who provide the material
support to carry out KDE e.V.'s activities.

<p>You can find official documents like the
<a href="/corporate/statutes.html">articles of association</a>, additional
<a href="/rules">rules and policies</a> or
<a href="/resources">forms</a> for activities as well.</p>

## Quick Links

<a href="https://jointhegame.kde.org"><img src="images/Jtg.png" class="noborder float-right" alt="Join the Game and support KDE" /></a>

<table class="qlblock" style="width: 500px;">
<tr>
<td>
  <div><a href="contact.php">Contact KDE e.V.</a></div>
  <div><a href="corporate/board.php">Board of Directors</a></div>
  <div><a href="corporate/statutes.php">Statutes of Association</a></div>
  <div><a href="whatiskdeev.php">Mission Statement</a></div>
  <div><a href="reports/">Reports</a></div>
</td>
<td>
  <div><a href="resources/supporting_member_application.pdf">New Supporting Member</a></div>
  <div><a href="resources/ev-questionnaire.text">New Member</a></div>
  <div><a href="members.php">Current Members</a></div>
</td>
</tr>
</table>
<div style="clear: both;"></div>

{% include blog.html %}
